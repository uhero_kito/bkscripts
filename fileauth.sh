. ${inc_prefix}fileauth.inc
. ${inc_prefix}usage.inc

case "$#" in
    0 )
        usage "<path>" >&2 ;;
    1 )
        fileauth "${1}" ;;
    * )
        while [ "${1}" ]; do
            echo "${1}:"
            fileauth "${1}"
            shift
        done
        ;;
esac