. ${inc_prefix}remove-dot-segments.inc

function fullpath {
    if [ ! "${1}" ]; then
        echo "Usage: fullpath <path>" >&2
        return 1
    fi
    
    if [ ${1:0:1} = "/" ]; then
        local testpath=${1}
    else
        local testpath=${PWD}/${1}
    fi
    
    result=`remove_dot_segments ${testpath}`
    if [ ${result} = "/" ]; then
        echo /
    else
        echo ${result%/}
    fi
}