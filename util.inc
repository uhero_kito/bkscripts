#
# 指定された文字列をトリミングします.
# 文字列の先頭または末尾に存在する1個以上のタブ文字(0x09), スペース(0x20)を削除し, 
# 残りの文字列を標準出力に表示します.
# このスクリプトは 0x09 がハードコーディングされているため, 修正時に置換しないよう気をつけてください.
#
function trim {
    echo "${1}" | sed 's/^[ 	]*//g' | sed 's/[ 	]*$//g'
}

#
# haystack の中に needle が含まれているかどうかを調べます.
# 含まれている場合はその位置を標準出力に出力し, ステータスコード 0 を返します.
# 含まれていない場合は何も表示せずにステータスコード 1 を返します.
# 
# needle に空文字列が指定された場合はステータスコード 1 を返します.
#
# @param $1 haystack
# @param $2 needle
#
function strpos {
    local haystack="${1}"
    local needle="${2}"
    
    if [ ! "${needle}" ]; then
        return 1
    fi
    
    local reg=`echo "${needle}" | sed 's;[\\^\\$\\.\\/\\[\\]];\\\\&;g'`
    local len=`expr "${haystack}" : ".*${reg}"`
    if [ ${len} -gt 0 ]; then
        expr ${len} - ${#needle}
        return
    else
        return 1
    fi
}

#
# もしも指定された文字列がコメント行または空行の場合はステータスコード 0 を返します.
# それ以外はステータスコード 1 を返します
#
function entryskip {
    local line=`trim "${1}"`
    if [ "${line:0:1}" = "#" ]; then
        return 0
    elif [ "${line}" = "" ]; then
        return 0
    else
        return 1
    fi
}

#
# 文字列の = から前の部分を取り出します
#
function entrykey {
    trim "`echo "${1}" | awk -F= '{print $1}'`"
}

#
# 文字列の = から後の部分を取り出します
#
function entryvalue {
    trim "`echo "${1}" | awk -F= '{print $2}'`"
}

#
# 文字列がアルファベット・数字・"_" , "-" の組み合わせから成り立つかどうかを判定します.
#
function validate_name {
    local matchcount=`expr "${1}" : "[a-zA-Z_\\-][a-zA-Z0-9_\\-]*"`
    if [ ${matchcount} = ${#1} ]; then
        return
    else
        return 1
    fi
}

#
# 文字列が [0-7][0-7][0-7] であらわされる正規表現に合致するかどうかを判定します.
#
function validate_mode {
    local matchcount=`expr "${1}" : "[0-7][0-7][0-7]"`
    if [ ${matchcount} = 3 ]; then
        return
    else
        return 1
    fi
}

#
# 指定されたコマンドを標準出力に表示し, それを実行します.
# 環境変数 DRY_RUN が設定されている場合は, コマンドを出力するだけで実行しません.
# バッチ内で cp, mv, rm などの基本コマンドを実行する際に使用します.
#
function run {
    echo $@
    if [ ! "${DRY_RUN}" ]; then
        $@
        return $?
    fi
}

#
# 指定されたコマンドが存在するかどうかを調べます.
# 存在する場合に 0, 存在しない場合に 1 を返します.
# 
# この関数は, Unix, Linux の各ディストリビューションごとの which の実装の違いを吸収します.
# 
# Unix (主に Solaris 等) の which コマンドでは, 結果が見つからなかった場合に
# 標準出力に "no ${1} in ${PATH}" 形式のエラーメッセージを出力します.
# (標準出力のみを使い, 標準エラー出力は使いません.)
#
# その他 Linux ディストリビューションの which コマンドでは, 
# 結果が見つからなかった場合に標準エラー出力にエラーメッセージを出力します.
# (標準エラー出力のみを使い, 標準出力には何も出力しません)
#
# 上記の挙動の違いを踏まえ, この関数では which の標準出力が / で始まっているかどうかを検査します.
# 標準入力が / で始まっている場合のみ 0 を返します. それ以外は 1 を返します.
#
function command_exists {
    local result=`which ${1} 2> /dev/null`
    if [ "${result}" ]; then
        if [ ${result:0:1} = "/" ]; then
            return 0
        fi
    fi
    return 1
}

if ! command_exists readlink; then
    function readlink {
        if [ -L "${1}" ]; then
            local detail=(`ls -la ${1}`)
            local last_index=`expr ${detail[@]} - 1`
            echo ${detail[${last_index}]}
            return 0
        else
            return 1
        fi
    }
fi
