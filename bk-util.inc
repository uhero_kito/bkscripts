. ${inc_prefix}util.inc

#
# 指定された文字列が, バックアップファイルの日付パート (YYYYMMDD_hhmm)
# として有効かどうかを判別します.
# 妥当な文字列の場合は 0, それ以外は 1 を返します.
# 
# この関数は, 変数 DRY_RUN の有無によって処理が変わります.
# もしも DRY_RUN が設定されている場合は "20110703_hhmm" のように, 
# 仮文字 (Y,M,D など) を使った表現も妥当とします.
# それ以外は, "20110703_1830" のように数値のみの場合だけ妥当とします.
#
function bk_validate_date {
    if [ "${DRY_RUN}" ]; then
        local reg="[1-2yY][09yY][0-9yY][0-9yY][0-1mM][0-9mM][0-3dD][0-9dD]_[0-2hH][0-9hH][0-5mM][0-9mM]"
    else
        local reg="[1-2][09][0-9][0-9][0-1][0-9][0-3][0-9]_[0-2][0-9][0-5][0-9]"
    fi
    local match=`expr "${OPTARG}" : "${reg}"`
    if [ ${match} = 13 ]; then
        return 0
    else
        return 1
    fi
}

#
# コロン (:) で区切られた除外対象文字列一覧を引数に取り, 
# それらをスペース区切りにして出力します.
# 引数に .svn が含まれていない場合は, 末尾に .svn を含めたものを出力します.
#
function bk_ignore_setup {
    local ignore=`trim ${1}`
    if [ ! ${ignore} ]; then
        echo ".svn"
        return
    fi
    
    local pos=`strpos "${1}"`
    if [ ! ${pos} ]; then
        ignore="${ignore%:}:.svn"
    fi
    echo ${ignore} | tr ":" " "
}

#
# 指定されたファイル名が, 除外対象かどうかを判別します.
# ファイル名の先頭または末尾が, 無視リストの項目に等しい場合は除外とします.
# 除外対象と判別された場合はマッチした文字列を出力します.
# 
# @param $1 filename    検査対象のファイル名
# @param $2 ignore_list 無視リスト一覧
#
function bk_ignore_match {
    local filename=`basename ${1}`
    local ignore_list=${2}
    for i in `echo ${ignore_list} | tr ':' ' '`; do
        if [ ${filename} = ${i} ]; then
            echo ${i}
        elif [ ${filename} = "${filename#${i}}" -a ${filename} = "${filename%${i}}" ]; then
            continue
        else
            echo ${i}
        fi
    done
}