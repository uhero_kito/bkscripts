#
# RFC3986 「5.2.4. ドットセグメントの削除」に基づき、
# 指定されたパスを正規化します。
# 二個以上連続した "/" は一個だけに変換されます。
#
function remove_dot_segments {
    if [ ! "${1}" ]; then
        echo "Usage: remove_dot_segments <path>" >&2
        return 1
    fi
    
    # 入力バッファは現在追加されているパス部品で初期化され、
    # 出力バッファは空の文字列にて初期化される。
    local input=${1}
    local output=""
    
    # 入力バッファが空でない間、以下をループする
    while [ ${input} ]; 
    do
        # 2個以上連続した "/" を 1 個だけに変換する
        while [ ${input:0:2} = "//" ]; do
            input=${input:1}
        done
        
        # 入力バッファの先頭部分が "../" や "./" で始まるならば、
        # 入力バッファからその部分を取り除く
        if [ ${input:0:3} = "../" ]; then
            input=${input:3}
        elif [ ${input:0:2} = "./" ]; then
            input=${input:2}
        
        # 入力バッファの先頭部分が "/./" や "/." で始まるならば、
        # "." が完全パスセグメントの場合、入力バッファ内のその部分を "/" に置き換える
        elif [ ${input:0:3} = "/./" ]; then
            input="/${input:3}"
        elif [ ${input} = "/." ]; then
            input="/"
        
        # 入力バッファの先頭部分が "/../" や "/.." で始まるならば、
        # ".." が完全パスセグメントの場合、入力バッファ内のその部分を "/" に置き換え、
        # 出力バッファから最後のセグメントと (ある場合) それに先行する "/" を削除する
        elif [ ${input:0:4} = "/../" ]; then
            input="/${input:4}"
            output=${output%/*}
        elif [ ${input} = "/.." ]; then
            input="/"
            output=${output%/*}
        
        # 入力バッファが "." や ".." のみから成るならば、
        # 入力バッファからそれを削除する
        elif [ ${input} = "." ] || [ ${input} = ".." ]; then
            input=""
        
        # 入力バッファの最初のセグメント、すなわち 
        # (ある場合) 最初の "/" 文字から、次の "/" を含まないそこまでの全ての文字、
        # あるいは入力バッファの最後を、出力バッファの最後へと移動する 
        else
            if [ ${input:0:1} = "/" ]; then
                input=${input:1}
                output=${output}/
            fi
            segment=`echo ${input} | awk -F/ '{print $1}'`
            input=${input:${#segment}}
            output=${output}${segment}
        fi
    done 
    echo ${output}
}