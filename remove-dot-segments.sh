. ${inc_prefix}remove-dot-segments.inc
. ${inc_prefix}usage.inc

usageopt="-h <path>"

while getopts "h" OPT; do
    case ${OPT} in
        "h")
            usage ${usageopt}
            exit 0
            ;;
        *)
            usage ${usageopt} >&2
            exit 1
            ;;
    esac
done
shift `expr ${OPTIND} - 1`

if [ ! "${1}" ]; then
    usage ${usageopt} >&2
    exit 1
fi
echo $*
while [ "${1}" ]; do
    remove_dot_segments ${1}
    shift
done