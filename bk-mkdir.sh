. ${inc_prefix}bk-mkdir.inc
. ${inc_prefix}usage.inc

options=()
usagearg="[OPTIONS]... PATH..."

while getopts "o:g:m:hn" OPT; do
    case ${OPT} in
    "o")
        options=("${options[@]}" "-o" ${OPTARG}) ;;
    "g")
        options=("${options[@]}" "-g" ${OPTARG}) ;;
    "m")
        options=("${options[@]}" "-m" ${OPTARG}) ;;
    "n")
        echo "テストモード (dry-run) で実行します."
        DRY_RUN=TRUE
        ;;
    "h")
        usage ${usagearg}
        exit
        ;;
    * )
        usage_short ${usagearg} >&2
        exit 1
        ;;
    esac
done
shift `expr ${OPTIND} - 1`

case "$#" in
0 )
    usage_short ${usagearg} >&2
    exit 1
    ;;
* )
    return_value=0
    bk_mkdir_init "${options[@]}"
    if [ $? != 0 ]; then
        bk_finalize
        exit 1
    fi
    while [ "${1}" ]; do
        echo "Target : ${1}"
        bk_mkdir ${1}
        case $? in
        0 )
            shift ;;
        2 )
            bk_finalize
            exit 1 ;;
        * )
            shift
            return_value=`expr ${return_value} + 1` ;;
        esac
        if [ "${1}" ]; then
            echo
        fi
    done
    ;;
esac

bk_finalize
exit ${return_value}