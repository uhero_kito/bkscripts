. ${inc_prefix}bkls.inc
. ${inc_prefix}bk-init.inc
. ${inc_prefix}bk-util.inc
. ${inc_prefix}util.inc

function bk_diff_setup {
    bk_init
    if [ $? != 0 ]; then
        return 1
    fi
    
    local arg_options=""
    local arg_oldcheck=""
    local arg_recursive=""
    local arg_date=""
    local arg_ignore=".svn"
    local arg_tag="bak"
    local arg_bkls_options=""
    local arg_quiet=""
    
    unset OPTIND
    while getopts "bworqd:i:t:" OPT; do
        case ${OPT} in
        "b" | "w" )
            arg_options="${arg_options} -${OPT}" ;;
        "o" )
            arg_oldcheck=TRUE ;;
        "q" )
            arg_quiet=TRUE ;;
        "r" )
            arg_recursive=TRUE ;;
        "i" )
            arg_ignore=`bk_ignore_setup ${OPTARG}` ;;
        "d" | "t" )
            arg_bkls_options="${arg_bkls_options} -${OPT} ${OPTARG}" ;;
        esac
    done
    
    if [ ${bk_diff_profile} ]; then
        local nextid=`expr ${bk_diff_profile} + 1`
    else
        local nextid=0
    fi
    
    bkls_setup ${arg_bkls_options[@]}
    if [ $? != 0 ]; then
        return 1
    fi
    
    bk_diff_bkls[${nextid}]=${bkls_profile}
    bk_diff_options[${nextid}]="${arg_options}"
    bk_diff_oldcheck[${nextid}]="${arg_oldcheck}"
    bk_diff_quiet[${nextid}]="${arg_quiet}"
    bk_diff_recursive[${nextid}]="${arg_recursive}"
    bk_diff_ignore[${nextid}]="${arg_ignore}"
    bk_diff_profile=${nextid}
}

function bk_diff {
    bk_init
    if [ $? != 0 ]; then
        return 3
    fi
    
    local target=${1}
    if [ -L ${target} ]; then
        target=`readlink ${target}`
    fi
    
    local index=${2:-0}
    if [ ! ${bk_diff_profile} -o ${bk_diff_profile} -lt ${index} ]; then
        echo "プロファイル番号 ${index} は登録されていません." >&2
        return 3
    fi
    
    local match=`bk_ignore_match ${target} ${bk_diff_ignore[${index}]}`
    if [ "${match}" ]; then
        echo "${target} => Skip. (matched: '${match}')"
        return
    fi
    
    if [ -d ${target} -a "${bk_diff_recursive[${index}]}" ]; then
        local return_value=0
        local lslist=`ls -a1 ${target}`
        for i in ${lslist}; do
            if [ ${i} = "." -o ${i} = ".." ]; then
                continue
            fi
                
            local nextsubject=${target%/}/${i}
            echo "Next   : ${nextsubject}"
            bk_diff ${nextsubject} ${index}
            case $? in
            1 )
                return_value=1 ;;
            3 )
                return 2 ;;
            esac
        done
        return ${return_value}
        
    elif [ ! -e "${target}" ]; then
        echo "ファイル '${target}' は見つかりませんでした." >&2
        return 2
        
    elif [ -f ${target} ]; then
        local bklist=`bkls ${target} ${bk_diff_bkls[${index}]}`
        if [ ! "${bklist}" ]; then
            return 2
        fi
        
        if [ ${bk_diff_oldcheck[${index}]} ]; then
            local number=1
            for i in ${bklist}; do
                local bkname=`basename ${i}`
                local result=`diff ${bk_diff_options[${index}]} ${i} ${target}`
                if [ "${result}" ]; then
                    echo "${number} 個目のバックアップで差分が見つかりました : ${bkname}"
                    if [ ! ${bk_diff_quiet[${index}]} ]; then
                        run diff ${bk_diff_options[${index}]} ${i} ${target}
                    fi
                    return 1
                fi
                number=`expr ${number} + 1`
            done
            echo "すべてのバックアップファイルが同一です。"
            return 0
        else
            for i in ${bklist}; do
                local bkname=`basename ${i}`
                echo "直近のバックアップが見つかりました : ${bkname}"
                local result=`diff ${bk_diff_options[${index}]} ${i} ${target}`
                if [ "${result}" ]; then
                    if [ ! ${bk_diff_quiet[${index}]} ]; then
                        run diff ${bk_diff_options[${index}]} ${i} ${target}
                    fi
                    return 1
                else
                    echo "変更箇所がありません。"
                    return 0
                fi
            done
        fi
    fi
}