function now {
    if [ ! "${1}" ]; then
        usage "${0} <format>" >&2
        return 1
    fi
    
    year=`date +%Y`
    month=`date +%m`
    date=`date +%d`
    hour=`date +%H`
    min=`date +%M`
    sec=`date +%S`
    result=`echo ${1} | sed "s/Y/${year}/g" | sed "s/M/${month}/g" | sed "s/D/${date}/g" | sed "s/h/${hour}/g" | sed "s/m/${min}/g" | sed "s/s/${sec}/g"`
    echo ${result}
}