. ${inc_prefix}now.inc
. ${inc_prefix}usage.inc

if [ ! "${1}" ]; then
    usage "<format>" >&2
    exit 1
fi
now "${1}"