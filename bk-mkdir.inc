. ${inc_prefix}bk-init.inc
. ${inc_prefix}fileauth.inc
. ${inc_prefix}fullpath.inc

function bk_mkdir_init {
    if [ "${bk_mkdir_init}" ]; then
        return
    fi
    
    bk_init
    if [ $? != 0 ]; then
        return 1
    fi
    
    bk_mkdir_owner=${BK_DEFAULT_OWNER}
    bk_mkdir_group=${BK_DEFAULT_GROUP}
    bk_mkdir_method=${BK_METHOD}
    
    unset OPTIND
    while getopts "o:g:m:" OPT; do
        case ${OPT} in
        "o")
            if validate_name ${OPTARG}; then
                bk_mkdir_owner=${OPTARG}
            else
                echo "オプション -o の値が不正です(${OPTARG})." >&2
                return 2
            fi
            ;;
        "g")
            if validate_name ${OPTARG}; then
                bk_mkdir_group=${OPTARG}
            else
                echo "オプション -g の値が不正です(${OPTARG})." >&2
                return 2
            fi
            ;;
        "m")
            case ${OPTARG} in
            "strict" | "rough")
                if bk_validate_method ${BK_AUTH} ${OPTARG}; then
                    bk_mkdir_method=${OPTARG}
                else
                    echo "オプション -m rough を指定するためには, auth の設定が user または root である必要があります." >&2
                    return 2
                fi
                ;;
            * )
                echo "オプション -m は strict, rough のどちらかを指定してください(${OPTARG})." >&2
                return 2
                ;;
            esac
            ;;
        * )
            echo "不明なオプションです(${OPT})" >&2
            return 2
            ;;
        esac
    done
    shift `expr ${OPTIND} - 1`
    
    case ${BK_AUTH} in
    "all" )
        bk_mkdir_mode=777 ;;
    "group" )
        bk_mkdir_mode=775 ;;
    "user" )
        bk_mkdir_mode=755 ;;
    esac
    
    bk_mkdir_init=TRUE
}

#
# 指定されたパスのバックアップ先ディレクトリを作成します.
# ディレクトリの生成に失敗した場合はステータスコード 1,
# オプションの指定が不正な場合はステータスコード 2 を返します.
#
function bk_mkdir {
    bk_mkdir_init
    if [ $? != 0 ]; then
        return 2
    fi
    
    # 引数が相対パスの場合は絶対パスに変換します
    local subject=${1}
    if [ "${1:0:1}" != "/" ]; then
        subject=`fullpath ${subject}`
    fi
    
    # 引数がディレクトリではない(あるいは存在しない)場合, 引数の親ディレクトリを操作対象とします.
    if [ ! -d ${subject} ]; then
        subject=`dirname ${subject}`
    fi
    
    # 作成対象のディレクトリが既に存在する場合は何もせずに正常終了します.
    local target="${BK_HOME}${subject}"
    if [ -e ${target} ]; then
        return
    fi
    
    case ${bk_mkdir_method} in
    "strict" )
        bk_mkdir_strict ${subject} ;;
    "rough" )
        bk_mkdir_rough ${subject} ;;
    * )
        return 2 ;;
    esac
}

function bk_mkdir_chall {
    if [ "${LOGNAME}" = "root" -a ${BK_AUTH} != "root" ]; then
        local subject=${1}
        local target="${BK_HOME}${subject}"
        if [ ${BK_AUTH} = "user" ]; then
            local fileauth=()
            fileauth=(`fileauth ${BK_HOME}`)
            local org_owner=${fileauth[1]}
            local org_group=${fileauth[2]}
            run chown ${org_owner}:${org_group} ${target}
        elif [ -d ${subject} ]; then
            local fileauth=()
            fileauth=(`fileauth ${subject}`)
            local org_owner=${fileauth[1]}
            local org_group=${fileauth[2]}
            run chown ${org_owner}:${org_group} ${target}
        else
            run chown ${bk_mkdir_owner}:${bk_mkdir_group} ${target}
        fi
    fi
}

function bk_mkdir_strict {
    local subject=${1}
    local target="${BK_HOME}${subject}"
    if [ ! -e ${target} ]; then
        # バックアップ先の親ディレクトリを作成します
        local basedir=`dirname ${subject}`
        if ! bk_mkdir_strict ${basedir}; then
            return 1
        fi
        
        # バックアップ先のディレクトリを作成します
        run mkdir -m ${bk_mkdir_mode} ${target}
        if [ $? != 0 ]; then
            return 1
        fi
        
        bk_mkdir_chall ${subject}
    fi
}

function bk_mkdir_rough {
    local subject=${1}
    local target="${BK_HOME}${subject}"
    if [ ! -e ${target} ]; then
        run mkdir -p ${target}
        if [ $? != 0 ]; then
            return 1
        fi
        
        bk_mkdir_chall ${subject}
    fi
}