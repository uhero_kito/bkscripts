. ${inc_prefix}bk-mkdir.inc
. ${inc_prefix}bk-diff.inc

#
# プロフィールを登録します.
#
# @param -d DATETIME バックアップの日時文字列を YYYYMMDD_hhmm 形式で指定します.
#                    あらかじめ bk_validate_date でバリデーションされている必要があります.
# @param -i LIST     無視するファイル名の先頭/末尾の文字列を指定します.
#                    複数の場合はコロン (:) で区切ります.
# @param -t TAG      バックアップファイルに付加するタグ名を指定します.
#                    未指定の場合のデフォルト値は 'bak' です.
#                    あらかじめ validate_name でバリデーションされている必要があります.
# @param -f          diff による差分確認をせず, 強制的にバックアップを行います.
# @param -r          bkup の引数にディレクトリを指定した際に,
#                    ディレクトリ内を再帰的にバックアップします.
# @param -b          diff の -b オプションです.
# @param -w          diff の -w オプションです.
#
function bkup_setup {
    bk_init
    if [ $? != 0 ]; then
        return 1
    fi
    
    local arg_date=""
    local arg_tag="bak"
    local arg_recursive=""
    local arg_force=""
    local arg_ignore=".svn"
    local arg_profile="default"
    local arg_diff_options=""
    
    unset OPTIND
    while getopts "d:i:t:bwfr" OPT; do
        case ${OPT} in
        "b" | "w" )
            arg_diff_options="${arg_diff_options} -${OPT}" ;;
        "d")
            arg_date=${OPTARG} ;;
        "i")
            arg_ignore=`bk_ignore_setup ${OPTARG}` ;;
        "t")
            arg_tag=${OPTARG} ;;
        "f")
            arg_force=TRUE ;;
        "r")
            arg_recursive=TRUE ;;
        esac
    done
    shift `expr ${OPTIND} - 1`
    
    if [ ! ${arg_date} ]; then
        arg_date=`now YMD_hm`
    fi
    
    if [ ${bkup_profile} ]; then
        local nextid=`expr ${bkup_profile} + 1`
    else
        local nextid=0
    fi
    
    bk_diff_setup -q -t ${arg_tag} ${arg_diff_options}
    if [ $? != 0 ]; then
        return 1
    fi
    
    bkup_diff[${nextid}]=${bk_diff_profile}
    bkup_datetime[${nextid}]=${arg_date}
    bkup_tag[${nextid}]=${arg_tag}
    bkup_force[${nextid}]=${arg_force}
    bkup_recursive[${nextid}]=${arg_recursive}
    bkup_ignore[${nextid}]=${arg_ignore}
    bkup_profile=${nextid}
    
    if [ ! ${bkup_profile} ]; then
        bkup_select_profile ${arg_profile}
    fi
}

#
# バックアップを実行します.
# @param $1 ファイル名
# @param $2 プロファイル番号 (省略した場合は 0 が指定される)
#
function bkup {
    bk_init
    if [ $? != 0 ]; then
        return 2
    fi
    
    local subject=${1}
    
    # 引数がシンボリックリンクの場合はリンク先のパスに変換します
    if [ -L ${subject} ]; then
        subject=`readlink ${subject}`
    fi
    
    # 引数が相対パスの場合は絶対パスに変換します
    if [ "${1:0:1}" != "/" ]; then
        subject=`fullpath ${subject}`
    fi
    
    local index=${2:-0}
    if [ ! ${bkup_profile} -o ${bkup_profile} -lt ${index} ]; then
        echo "プロファイル番号 ${index} は登録されていません. はじめに bkup_setup を実行してください." >&2
        return 2
    fi
    
    # ファイル名が無視リストにマッチしていた場合はスキップ(正常終了)します
    local ignore=${bkup_ignore[${index}]}
    local match=`bk_ignore_match ${subject} ${ignore}`
    if [ ${match} ]; then
        echo "${subject} => Skip. (matched: '${match}')"
        return
    fi
    
    # ディレクトリの場合, もしも -r オプションが付いていればその中を再帰的にバックアップします
    # -r オプションがない場合はスキップします.
    if [ -d ${subject} ]; then
        local recursive=${bkup_recursive[${index}]}
        if [ ${recursive} ]; then
            bk_mkdir ${subject}
            local lslist=`ls -a1 ${subject}`
            for i in ${lslist}; do
                if [ ${i} = "." -o ${i} = ".." ]; then
                    continue
                fi
                
                local nextsubject=${subject%/}/${i}
                bkup ${nextsubject}
                if [ $? = 2 ]; then
                    return 1
                fi
            done
        else
            echo "${subject} => Skip. (directory)"
        fi
    # ファイルの場合はまず差分を調べ, 差分があった場合に cp -pi でコピーします
    elif [ -f ${subject} ]; then
        local tag=${bkup_tag[${index}]}
        local datetime=${bkup_datetime[${index}]}
        if [ ${bkup_force[${index}]} ]; then
            bk_mkdir ${subject}
            run cp -pi ${subject} ${BK_HOME}${subject}-${datetime}-${tag}
        else
            local diff_profile=${bkup_diff[${index}]}
            bk_diff ${subject} ${diff_profile} 2> /dev/null
            case $? in
            0 )
                return ;;
            1 | 2 )
                echo "バックアップを行います。"
                bk_mkdir ${subject}
                run cp -pi ${subject} ${BK_HOME}${subject}-${datetime}-${tag}
                ;;
            * )
                return 2 ;;
            esac
        fi
    # それ以外の場合はファイルが見つからなかった場合なので, ステータスコード 1 を返します
    else
        echo "${subject} => Skip. (File not found.)"
        return 1
    fi
}