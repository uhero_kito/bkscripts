#
# コマンドの使用方法を出力します
#
function usage_short {
    local command=`basename ${0}`
    echo "Usage: ${command} $*"
    echo "詳しくは '${command} -h' を実行してください."
    echo
}

#
# コマンドの使用方法と説明文を出力します
#
function usage {
    local command=`basename ${0}`
    echo "Usage: ${command} $*"
    echo
    if [ -f ${0}.txt ]; then
        cat ${0}.txt
        echo
        echo
    fi
}