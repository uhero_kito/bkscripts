. ${inc_prefix}fullpath.inc
. ${inc_prefix}usage.inc

usageopt="[-h] PATH..."

while getopts "h" OPT; do
    case ${OPT} in
        "h")
            usage ${usageopt}
            exit 0
            ;;
        * )
            usage-short ${usageopt} >&2
            exit 1
            ;;
    esac
done
shift `expr ${OPTIND} - 1`

if [ ! "${1}" ]; then
    usage_short ${usageopt} >&2
    exit 1
fi

while [ "${1}" ]; do
    fullpath ${1}
    shift
done