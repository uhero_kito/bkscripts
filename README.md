# bkscripts

Shell script library for local resource versioning.

## Install

```
 # git clone https://gitlab.com/uhero_kito/bkscripts.git /usr/local/scripts
 # bash /usr/local/scripts/configure
```

## Usage

### bkup

Makes backup files.

Command example:

```
 # cd /etc/httpd/conf/
 # bkup httpd.conf
```

Output:

```
 START  : 2015-06-15 09:36:43
 HOME   : /var/bkup
 AUTH   : all
 METHOD : strict
 Tag    : bak
 Ignore : .svn
 Target : httpd.conf
 バックアップを行います。
 mkdir -m 777 /var/bkup/etc/httpd/conf
 chown root:root /var/bkup/etc/httpd/conf
 cp -pi /etc/httpd/conf/httpd.conf /var/bkup/etc/httpd/conf/httpd.conf-20150615_0936-bak
 FINISH : 2015-06-15 09:36:43
```

You can read command line options by -h

```
 # bkup -h
```

### bk-diff

Shows diff of recent backup file.

Command example:

```
 # bk-diff /etc/httpd/conf/httpd.conf
```

Output:

```
 START  : 2015-06-15 09:41:59
 HOME   : /var/bkup
 AUTH   : all
 METHOD : strict
 Tag    : bak
 Ignore : .svn
 Target : preview.uhero.co.jp.vconf
 1 個目のバックアップで差分が見つかりました : /etc/httpd/conf/httpd.conf-20150615_0936-bak
 diff /var/bkup/etc/httpd/conf/httpd.conf-20150615_0936-bak /etc/httpd/conf/httpd.conf
 789c789
 <     AllowOverride None
 ---
 >     AllowOverride All
 FINISH : 2015-06-15 09:41:59
```

You can read command line options by -h

```
 # bk-diff -h
```

### fullpath

Shows full path of argument.

Command example:

```
 # cd /etc
 # cd httpd
 # cd conf
 # fullpath httpd.conf
```

Output:

```
 /etc/httpd/conf/httpd.conf
```
