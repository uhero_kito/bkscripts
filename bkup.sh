. ${inc_prefix}bk-mkdir.inc
. ${inc_prefix}bkup.inc
. ${inc_prefix}usage.inc

usagearg="[OPTIONS]... FILE..."
mkdir_options=()
bkup_options=()

while getopts "o:g:m:d:t:i:bwrnh" OPT; do
    case ${OPT} in
    "o")
        mkdir_options=("${mkdir_options[@]}" "-o" ${OPTARG}) ;;
    "g")
        mkdir_options=("${mkdir_options[@]}" "-g" ${OPTARG}) ;;
    "m")
        mkdir_options=("${mkdir_options[@]}" "-m" ${OPTARG}) ;;
    "d")
        bkup_options=("${bkup_options[@]}" "-d" ${OPTARG}) ;;
    "t")
        bkup_options=("${bkup_options[@]}" "-t" ${OPTARG}) ;;
    "i")
        bkup_options=("${bkup_options[@]}" "-i" ${OPTARG}) ;;
    "b" | "w" | "r" )
        bkup_options=("${bkup_options[@]}" -${OPT}) ;;
    "n")
        echo "テストモード (dry-run) で実行します."
        DRY_RUN=TRUE
        ;;
    "h")
        usage ${usagearg}
        exit
        ;;
    * )
        usage_short ${usagearg} >&2
        exit 1
        ;;
    esac
done
shift `expr ${OPTIND} - 1`

case "$#" in
0 )
    usage_short ${usagearg} >&2
    exit 1
    ;;
* )
    return_value=0
    bk_mkdir_init "${mkdir_options[@]}"
    if [ $? != 0 ]; then
        bk_finalize
        return 1
    fi
    
    bkup_setup "${bkup_options[@]}"
    if [ $? != 0 ]; then
        bk_finalize
        return 1
    fi
    
    echo "Tag    : ${bkup_tag}"
    echo "Ignore : ${bkup_ignore}"
    while [ "${1}" ]; do
        echo "Target : ${1}"
        bkup ${1}
        case $? in
        0 )
            shift ;;
        2 )
            bk_finalize
            exit 1 ;;
        * )
            shift
            return_value=`expr ${return_value} + 1` ;;
        esac
        if [ "${1}" ]; then
            echo
        fi
    done
esac

bk_finalize
exit ${return_value}