. ${inc_prefix}usage.inc
. ${inc_prefix}bkls.inc

usagearg="[-d DATETIME] [-t TAGNAME] FILE"
options=()

while getopts "d:t:hq" OPT; do
    case ${OPT} in
    "d" )
        if bk_validate_date "${OPTARG}"; then
            options=(${options[@]} -d ${OPTARG})
        else
            echo "-d オプションが不正です. 'YYYYMMDD_hhmm' 形式で指定してください. ('${OPTARG}')" >&2
            exit 1
        fi
        ;;
    "t" )
        if validate_name "${OPTARG}"; then
            options=(${options[@]} -t ${OPTARG})
        else
            echo "-t オプションが不正です. 妥当なタグ名を指定してください. ('${OPTARG}')" >&2
            exit 1
        fi
        ;;
    "h" )
        usage ${usagearg}
        exit
        ;;
    * )
        usage_short ${usagearg} >&2
        exit 1
        ;;
    esac
done
shift `expr ${OPTIND} - 1`

case "$#" in
0 )
    usage_short ${usagearg} >&2
    exit 1
    ;;
* )
    bkls_setup "${options[@]}"
    if [ $? != 0 ]; then
        bk_finalize
        exit 1
    fi
    
    if [ ${bkls_tag[${bkls_profile}]} ]; then
        echo "Tag    : ${bkls_tag[${bkls_profile}]}"
    fi
    if [ ${bkls_datetime[${bkls_profile}]} ]; then
        echo "Filter : ${bkls_tag[${bkls_profile}]}"
    fi
    
    return_value=0
    while [ "${1}" ]; do
        echo "Target : ${1}"
        bkls ${1}
        case $? in
        0 )
            shift ;;
        1 )
            return_value=`expr ${return_value} + 1`
            shift
            ;;
        2 )
            bk_finalize
            exit 1
            ;;
        esac
        if [ "${1}" ]; then
            echo
        fi
    done
    ;;
esac

bk_finalize
exit ${return_value}
