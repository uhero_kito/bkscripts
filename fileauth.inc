#
# "rwx" 形式の文字列を 0 から 7 までの数値に変換します.
#
function rwx2int {
    local value=0
    if [ ${1:0:1} != "-" ]; then
        value=`expr ${value} + 4`
    fi
    if [ ${1:1:1} != "-" ]; then
        value=`expr ${value} + 2`
    fi
    if [ ${1:2:1} != "-" ]; then
        value=`expr ${value} + 1`
    fi
    echo ${value}
}

#
# 指定されたモードを数値表現で出力します.
#
function mode2int {
    if [ ${#1} -lt 10 ]; then
        echo "'-rwxrwxrwx' 形式の文字列のみ有効です。" >&2
    exit 1
    fi
    
    local u=`rwx2int ${1:1:3}`
    local g=`rwx2int ${1:4:3}`
    local o=`rwx2int ${1:7:3}`
    echo ${u}${g}${o}
}

#
# 指定されたファイルのパーミッション, ユーザー, グループ情報を
# スペース区切りで出力します.
# パーミッションは 3 桁の数値で出力します.
#
function fileauth {
    if [ ! -e "${1}" ]; then
        echo "ファイル '${1}' が存在しません。" >&2
        return 1
    fi
    local INFO=()
    INFO=(`ls -lad ${1} | awk '{print $1, $3, $4}'`)
    INFO[0]=`mode2int ${INFO[0]}`
    echo ${INFO[@]}
}