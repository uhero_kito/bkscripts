. ${inc_prefix}bk-init.inc
. ${inc_prefix}bk-util.inc

#
# bkls 関数の環境設定を行います.
# この関数は, 設定時に割り当てられたプロファイル番号 (0 以上の整数) を変数 bkls_profile に設定します.
# このプロファイル番号は bkls 関数の第二引数で使用されます.
#
# オプション -d, -t の引数はあらかじめ bk_validate_date あるいは validate_name でバリデーションしてください.
#
# @param -d バリデーション済の日付文字列
# @param -t バリデーション済のタグ名
#
function bkls_setup {
    bk_init
    if [ $? != 0 ]; then
        return 1
    fi
    
    local arg_tag=""
    local arg_date=""
    
    unset OPTIND
    while getopts "d:t:" OPT; do
        case ${OPT} in
        "d" )
            arg_date=${OPTARG} ;;
        "t" )
            arg_tag=${OPTARG}  ;;
        esac
    done
    
    if [ ${bkls_profile} ]; then
        local nextid=`expr ${bkls_profile} + 1`
    else
        local nextid=0
    fi
    
    bkls_tag[${nextid}]=${arg_tag}
    bkls_date[${nextid}]=${arg_date}
    bkls_profile=${nextid}
}

#
# @param $1 ファイル名
# @param $2 プロファイル番号. (省略した場合は 0 が指定される)
#
function bkls {
    bk_init
    if [ $? != 0 ]; then
        return 2
    fi
    
    local target=${1}
    if [ -L ${target} ]; then
        target=`readlink ${target}`
    fi
    
    local index=${2:-0}
    if [ ! ${bkls_profile} -o ${bkls_profile} -lt ${index} ]; then
        echo "プロファイル番号 ${index} は登録されていません. はじめに bkls_setup を実行してください." >&2
        return 2
    fi
    if [ -d "${target}" ]; then
        echo "引数 '${target}' はディレクトリです. ファイルを指定してください." >&2
        return 1
    fi
    
    local tag=${bkls_tag[${index}]}
    local date=${bkls_date[${index}]}
    local bkpath=${BK_HOME}`fullpath ${target}`
    local errormsg="バックアップがありません。"
    if [ ${tag} ]; then
        local pattern="${bkpath}-????????_????-${tag}"
    else
        local pattern="${bkpath}-????????_????-*"
    fi
    if [ ${date} ]; then
        local border=${bkpath}-${date}
        ls -1r ${pattern} 2> /dev/null | awk '{if ($1 < "'${border}'") { print $1 } }'
        ls ${pattern} > /dev/null 2> /dev/null
    else
        ls -1r ${pattern} 2> /dev/null
    fi
    if [ $? != 0 ]; then
        echo ${errormsg} >&2
        return 1
    fi
}