. ${inc_prefix}util.inc
. ${inc_prefix}now.inc
. ${inc_prefix}fileauth.inc

#
# auth と method の組み合わせが妥当かどうかを判定します.
# 以下の条件を両方とも満たす場合は不正とし, ステータスコード 1 (エラー) を返します.
# 
# - auth が "all" または "group" のどちらか
# - method が "rough"
#
# それ以外はステータスコード 0 を返します.
#
# @param $1 auth の値
# @param $2 method の値
#
function bk_validate_method {
    local auth=${1}
    local method=${2}
    case ${auth} in
    "all" | "group" )
        if [ ${method} = "rough" ]; then
            return 1
        fi
    esac
}

#
# @param val ディレクトリ名
#
function bk_init_set_home {
    local val=${1}
    local bkdir=`fullpath ${val}`
    if [ "${val:0:1}" != "/" ]; then
        echo "backupdir: 絶対パスを指定してください(${val})." >&2
        return 1
    elif [ ! -d ${bkdir} ]; then
        echo "backupdir: ディレクトリ '${bkdir}' が見つかりません." >&2
        return 1
    elif [ ${bkdir} = "/" ]; then
        echo "ルートディレクトリ (/) はバックアップ先として利用できません." >&2
        return 1
    else
        BK_HOME=${bkdir}
    fi
}

#
# @param key キー (auth, method, default_owner, default_group のどれか)
# @param val 値
#
function bk_init_set_config {
    local key=${1}
    local val=${2}
    case ${key} in
    "auth")
        case ${val} in
        "all" | "root" | "group" | "user" )
            BK_AUTH=${val} ;;
        * )
            echo "キー auth が不正です(${val}). all, root, group, user のいずれかを指定してください." >&2
            return 1
            ;;
        esac
        ;;
    "method")
        case ${val} in
        "strict" | "rough" )
            BK_METHOD=${val} ;;
        * )
            echo "キー method が不正です(${val}). strict, rough を指定してください." >&2
            return 1
            ;;
        esac
        ;;
    "default_owner")
        if validate_name ${val}; then
            BK_DEFAULT_OWNER=${val}
        else
            echo "キー default_owner が不正です(${val}). ユーザー名として適切な値を指定してください." >&2
            return 1
        fi
        ;;
    "default_group")
        if validate_name ${val}; then
            BK_DEFAULT_GROUP=${val}
        else
            echo "キー default_group が不正です(${val}). グループ名として適切な値を指定してください." >&2
            return 1
        fi
        ;;
    * )
        echo "不明な設定項目名です(${key}). auth, method, default-owner, default-group を指定してください." >&2
        return 1
        ;;
    esac
}

function bk_init_set_master {
    if entryskip "${1}"; then
        return
    fi
    
    local key=`entrykey   "${1}"`
    local val=`entryvalue "${1}"`
    case ${key} in
    "backupdir")
        bk_init_set_home ${val}
        return $?
        ;;
    "auth" | "method" | "default_group" | "default_owner" )
        bk_init_set_config ${key} ${val}
        return $?
        ;;
    * )
        echo "不明な設定項目名です(${key}). backupdir, auth, method, default_group, default_owner を指定してください." >&2
        return 1
        ;;
    esac
}

function bk_init_set_local {
    if entryskip "${1}"; then
        return
    fi
    
    local key=`entrykey   "${1}"`
    local val=`entryvalue "${1}"`
    bk_init_set_config ${key} ${val}
}

#
# バックアップ関連の初期化処理を行います
#
function bk_init {
    if [ "${BK_INIT}" ]; then
        return
    fi
    
    BK_HOME="/var/bkup"
    BK_AUTH="all"
    BK_METHOD="strict"
    BK_DEFAULT_OWNER="root"
    BK_DEFAULT_GROUP="root"
    BK_QUIET_MODE=""
    GROUP=`groups`
    
    unset OPTIND
    while getopts "q" OPT; do
        case ${OPT} in
        "q" )
            BK_QUIET_MODE=TRUE ;;
        esac
    done
    
    # master 設定をロードします
    local masterconf=`dirname ${0}`/bkup.ini
    local error_found=""
    if [ -f ${masterconf} ]; then
        while read line; do
            bk_init_set_master "${line}"
            if [ $? != 0 ]; then
                error_found=TRUE
            fi
        done < ${masterconf}
        if [ ${error_found} ]; then
            echo
            echo "${masterconf} の設定エラーです." >&2
            echo
            return 1
        fi
    fi
    
    # 環境変数でバックアップディレクトリが指定されている場合は上書きします
    if [ "${BKUP_DIR}" ]; then
        if [ ${BKUP_DIR:0:1} = "/" ]; then
            BK_HOME=`fullpath ${BKUP_DIR}`
        else
            echo "環境変数 BKUP_DIR には絶対パスを指定してください. (${BKUP_DIR})" >&2
            return 1
        fi
    fi
    
    # local 設定をロードします
    local localconf=${BK_HOME%/}/.config
    if [ -f ${localconf} ]; then
        while read line; do
            bk_init_set_local "${line}"
            if [ $? = 1 ]; then
                error_found=TRUE
            fi
        done < ${localconf}
        if ! bk_validate_method ${BK_AUTH} ${BK_METHOD}; then
            echo "オプション -m rough を指定するためには, auth の値を user または root に設定する必要があります." >&2
            error_found=TRUE
        fi
        if [ ${error_found} ]; then
            echo
            echo "${localconf} の設定エラーです." >&2
            echo
            return 1
        fi
    fi
    
    # バリデーション
    if [ ! -d "${BK_HOME}" ]; then
        echo "バックアップディレクトリ '${BK_HOME}' が見つかりません." >&2
        return 1
    fi
    
    local fileauth=()
    fileauth=(`fileauth ${BK_HOME}`)
    case ${BK_AUTH} in
    "root" )
        if [ "${LOGNAME}" != "root" ]; then
            echo "実行するには root 権限が必要です." >&2
            return 1
        fi
        ;;
    "user" )
        local dirowner=${fileauth[1]}
        if [ "${LOGNAME}" != "${dirowner}" -a "${LOGNAME}" != "root" ]; then
            echo "この操作はユーザー '${dirowner}' 専用です." >&2
            return 1
        fi
        ;;
    "group" )
        local dirgroup=${fileauth[2]}
        if [ "${GROUP}" != "${dirgroup}" -a "${LOGNAME}" != "root" ]; then
            echo "この操作はグループ '${dirgroup}' 専用です." >&2
            return 1
        fi
        ;;
    esac
    
    if [ ! "${BK_QUIET_MODE}" ]; then
        echo "START  : `now 'Y-M-D h:m:s'`"
        echo "HOME   : ${BK_HOME}"
        echo "AUTH   : ${BK_AUTH}"
        echo "METHOD : ${BK_METHOD}"
    fi
    
    BK_INIT=TRUE
}

function bk_finalize {
    if [ "${BK_INIT}" ]; then
        if [ ! "${BK_QUIET_MODE}" ]; then
            echo "FINISH : `now 'Y-M-D h:m:s'`"
            echo
        fi
        unset BK_INIT
        unset BK_HOME
        unset BK_AUTH
        unset BK_METHOD
        unset BK_DEFAULT_OWNER
        unset BK_DEFAULT_GROUP
        unset BK_QUIET_MODE
    fi
}