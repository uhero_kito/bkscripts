. ${inc_prefix}usage.inc
. ${inc_prefix}util.inc
. ${inc_prefix}bk-util.inc
. ${inc_prefix}bk-diff.inc

usagearg="[OPTIONS]... [FILE]..."
options=()
arg_datetime=""
arg_tag="bak"

while getopts "bworqd:i:t:h" OPT; do
    case ${OPT} in
    "b" | "w" | "o" | "r" | "q" )
        options=("${options[@]}" -${OPT}) ;;
    "d" )
        if bk_validate_date "${OPTARG}"; then
            options=(${options[@]} -d ${OPTARG})
            arg_datetime=${OPTARG}
        else
            echo "-d オプションが不正です. 'YYYYMMDD_hhmm' 形式を指定してください. ('${OPTARG}')" >&2
            exit 1
        fi
        ;;
    "t" )
        if validate_name "${OPTARG}"; then
            options=(${options[@]} -t ${OPTARG})
            arg_tag=${OPTARG}
        else
            echo "-t オプションが不正です. 適切なタグ名を指定してください. ('${OPTARG}')" >&2
            exit 1
        fi
        ;;
    "i" )
        options=("${options[@]}" -i "${OPTARG}") ;;
    "h" )
        usage ${usagearg}
        exit
        ;;
    * )
        usage_short ${usagearg} >&2
        exit 1
        ;;
    esac
done
shift `expr ${OPTIND} - 1`

case "$#" in
0 )
    usage_short ${usagearg} >&2
    exit 1
    ;;
* )
    bk_diff_setup "${options[@]}" -t ${arg_tag}
    if [ $? != 0 ]; then
        bk_finalize
        exit 1
    fi
    
    # オプション指定の内容を標準出力に表示します
    optiondesc=()
    if [ "${bk_diff_recursive[${bk_diff_profile}]}" ]; then
        optiondesc=("recursive")
    fi
    if [ "${bk_diff_oldcheck[${bk_diff_profile}]}" ]; then
        optiondesc=(${optiondesc[@]} "old-check")
    fi
    echo "Tag    : ${arg_tag}"
    echo "Ignore : ${bk_diff_ignore[${bk_diff_profile}]}"
    if [ "${arg_datetime}" ]; then
        echo "Filter : ${arg_datetime}"
    fi
    if [ 0 -lt ${#optiondesc} ]; then
        echo "Option : ${optiondesc[@]}"
    fi
    
    return_value=0
    while [ "${1}" ]; do
        echo "Target : ${1}"
        bk_diff "${1}"
        case $? in
        0 )
            shift ;;
        1 )
            if [ ${return_value} -lt 2 ]; then
                return_value=1
            fi
            shift
            ;;
        2 )
            shift ;;
        * )
            bk_finalize
            exit 2
            ;;
        esac
        if [ "${1}" ]; then
            echo
        fi
    done
    ;;
esac
bk_finalize
exit ${return_value}